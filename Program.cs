﻿Student student = new Student();
student.InputInfor();

Console.WriteLine("\nThong tin sinh vien:");
student.DisplayInfor();

class Student
{
    public int Id;
    public string Name;
    public int Age;
    public double GPA;

    public void InputInfor()
    {
        Console.Write("Nhap ma sinh vien: ");
        Id = int.Parse(Console.ReadLine());

        Console.Write("Nhap ten sinh vien: ");
        Name = Console.ReadLine();

        Console.Write("Nhap tuoi: ");
        Age = int.Parse(Console.ReadLine());

        Console.Write("Nhap diem trung binh: ");
        GPA = double.Parse(Console.ReadLine());
    }

    public void DisplayInfor()
    {
        Console.WriteLine("Ma sinh vien: " + Id);
        Console.WriteLine("Ten sinh vien: " + Name);
        Console.WriteLine("Tuoi sinh vien: " + Age);
        Console.WriteLine("Diem trung binh: " + GPA);
        CalculateGrade();
    }

    private void CalculateGrade()
    {
        if (GPA >= 8.0)
        {
            Console.WriteLine("Xep loai: Gioi");
        }
        else if (GPA >= 6.5)
        {
            Console.WriteLine("Xep loai: Kha");
        }
        else if (GPA >= 5.0)
        {
            Console.WriteLine("Xep loai: Trung binh");
        }
        else
        {
            Console.WriteLine("Xep loai: Yeu");
        }
    }
}
